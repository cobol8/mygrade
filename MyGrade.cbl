       IDENTIFICATION DIVISION. 
       PROGRAM-ID. MYGRADE.
       AUTHOR. ARTHORN.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION   IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01  SCORE-DETAIL.
           88 END-OF-SCORE-FILE VALUE HIGH-VALUE.
           05 COURSE-ID         PIC X(6).
           05 COURSE-NAME       PIC X(99).
           05 CREDIT            PIC X(1).
           05 GRADE             PIC X(2).
       
       PROCEDURE DIVISION.
       BEGIN.
           OPEN OUTPUT SCORE-FILE 
           
           MOVE "202101" TO COURSE-ID 
           MOVE "Information Services and Study Fundamentals" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "B+" TO GRADE 
           WRITE SCORE-DETAIL 

           MOVE "212101" TO COURSE-ID 
           MOVE "English I" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL 

           MOVE "302111" TO COURSE-ID 
           MOVE "Calculus and Analytic Geometry I" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL 

           MOVE "303101" TO COURSE-ID 
           MOVE "Chemistry I" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "303102" TO COURSE-ID 
           MOVE "Chemistry Laboratory I" 
           TO COURSE-NAME 
           MOVE "1" TO CREDIT 
           MOVE "A" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "306101" TO COURSE-ID 
           MOVE "General Biology I" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "306102" TO COURSE-ID 
           MOVE "General Biology Laboratory I" 
           TO COURSE-NAME 
           MOVE "1" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "308101" TO COURSE-ID 
           MOVE "Introductory Physics I" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "308102" TO COURSE-ID 
           MOVE "Introductory Physics Laboratory I" 
           TO COURSE-NAME 
           MOVE "1" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "212102" TO COURSE-ID 
           MOVE "English II" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "302112" TO COURSE-ID 
           MOVE "Calculus and Analytic Geometry II" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "303103" TO COURSE-ID 
           MOVE "Chemistry II" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "303104" TO COURSE-ID 
           MOVE "Chemistry Laboratory II" 
           TO COURSE-NAME 
           MOVE "1" TO CREDIT 
           MOVE "B+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "306103" TO COURSE-ID 
           MOVE "General Biology II" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "306104" TO COURSE-ID 
           MOVE "General Biology Laboratory II" 
           TO COURSE-NAME 
           MOVE "1" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "308103" TO COURSE-ID 
           MOVE "Introductory Physics II" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "308104" TO COURSE-ID 
           MOVE "Introductory Physics Laboratory II" 
           TO COURSE-NAME 
           MOVE "1" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310201" TO COURSE-ID 
           MOVE "Computer and Data Processing" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "A" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "203100" TO COURSE-ID 
           MOVE "Man and Civilization" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "212301" TO COURSE-ID 
           MOVE "Reading Laboratory I" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "226100" TO COURSE-ID 
           MOVE "Man and Economy" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "302213" TO COURSE-ID 
           MOVE "Calculus and Analytic Geometry III" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "D+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "302323" TO COURSE-ID 
           MOVE "Linear Algebra I" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "D" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310211" TO COURSE-ID 
           MOVE "Computer Science I" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310212" TO COURSE-ID 
           MOVE "Discrete Mathematics" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "312201" TO COURSE-ID 
           MOVE "Elementary Statistics" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "208101" TO COURSE-ID 
           MOVE "Thai I" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "212302" TO COURSE-ID 
           MOVE "Reading Laboratory II" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "224101" TO COURSE-ID 
           MOVE "Man and Politics" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "225100" TO COURSE-ID 
           MOVE "Man, Society and Culture" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "302315" TO COURSE-ID 
           MOVE "Ordinary Differential Equations" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310213" TO COURSE-ID 
           MOVE "Computer Science II" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310231" TO COURSE-ID 
           MOVE "Computer Oganization and Assembly Language" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "A" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310341" TO COURSE-ID 
           MOVE "Data Structure and Algorithms" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "441101" TO COURSE-ID 
           MOVE "Wellness Development" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "601101" TO COURSE-ID 
           MOVE "Man and Aesthetics" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "226111" TO COURSE-ID 
           MOVE "Introduction to Economics" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "230416" TO COURSE-ID 
           MOVE "Production Management" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "D" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "308371" TO COURSE-ID 
           MOVE "Introduction to Electronics" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310332" TO COURSE-ID 
           MOVE "Computer Architecture" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310444" TO COURSE-ID 
           MOVE "File Processing" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "A" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310482" TO COURSE-ID 
           MOVE "Computer Graphic" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "301301" TO COURSE-ID 
           MOVE "Quality Management" 
           TO COURSE-NAME 
           MOVE "1" TO CREDIT 
           MOVE "D" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310327" TO COURSE-ID 
           MOVE "Principles of Programming Languages" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "A" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310342" TO COURSE-ID 
           MOVE "Database Design" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310414" TO COURSE-ID 
           MOVE "Software Engineering" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310453" TO COURSE-ID 
           MOVE "Operating System" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "A" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310461" TO COURSE-ID 
           MOVE "Data Communication" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "441113" TO COURSE-ID 
           MOVE "Basketball I" 
           TO COURSE-NAME 
           MOVE "1" TO CREDIT 
           MOVE "A" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310452" TO COURSE-ID 
           MOVE "Compiler Construction" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310462" TO COURSE-ID 
           MOVE "Computer Networks" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "A" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310481" TO COURSE-ID 
           MOVE "Systems Analysis and Design" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310484" TO COURSE-ID 
           MOVE "Introduction to Artificial Intelligence" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310485" TO COURSE-ID 
           MOVE "Office Automation Management" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "C+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310492" TO COURSE-ID 
           MOVE "Computer Seminar" 
           TO COURSE-NAME 
           MOVE "1" TO CREDIT 
           MOVE "B+" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310417" TO COURSE-ID 
           MOVE "Selected Topics" 
           TO COURSE-NAME 
           MOVE "3" TO CREDIT 
           MOVE "B" TO GRADE 
           WRITE SCORE-DETAIL

           MOVE "310491" TO COURSE-ID 
           MOVE "Projects in Computer Science" 
           TO COURSE-NAME 
           MOVE "2" TO CREDIT 
           MOVE "A" TO GRADE 
           WRITE SCORE-DETAIL

           

           CLOSE SCORE-FILE 
           GOBACK 
           .
