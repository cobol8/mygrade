       IDENTIFICATION DIVISION. 
       PROGRAM-ID. GRADE.
       AUTHOR. ARTHORN.

       ENVIRONMENT DIVISION. 
       INPUT-OUTPUT SECTION. 
       FILE-CONTROL. 
           SELECT SCORE-FILE ASSIGN TO "score.dat"
              ORGANIZATION IS LINE SEQUENTIAL.
           SELECT AVG-FILE ASSIGN TO "avg.txt"
              ORGANIZATION IS LINE SEQUENTIAL.

       DATA DIVISION. 
       FILE SECTION. 
       FD  SCORE-FILE.
       01 SCORE-DETAIL.
          88 END-OF-SCORE-FILE                VALUE HIGH-VALUE.
          05 COURSE-ID          PIC X(6).
          05 COURSE-NAME        PIC X(99).
          05 CREDIT             PIC 9(1)      VALUE ZERO.
          05 GRADE              PIC X(2).
       FD  AVG-FILE.
       01 AVG-DETAIL.
           05 AVG-GRADE1             PIC 9.999 VALUE ZERO.
           
       WORKING-STORAGE SECTION. 
       01 SCORE                 PIC 9(2)V9(3) VALUE ZERO.
       01 SCORE-ALL             PIC 9(3)V9(3) VALUE ZERO.
       01 SUM-UNIT              PIC 9(3)      VALUE ZERO.
       01 SCORE2                PIC 9(2)V9(3) VALUE ZERO.
       01 SCORE-ALL2            PIC 9(3)V9(3) VALUE ZERO.
       01 SUM-UNIT2             PIC 9(3)      VALUE ZERO.  
       01 SCORE3                PIC 9(2)V9(3) VALUE ZERO.
       01 SCORE-ALL3            PIC 9(3)V9(3) VALUE ZERO.
       01 SUM-UNIT3             PIC 9(3)      VALUE ZERO.
       01 AVG-GRADE             PIC 9(2)V9(3) VALUE ZERO.
       01 AVG-SCI-GRADE         PIC 9(2)V9(3) VALUE ZERO.
       01 AVG-CS-GRADE          PIC 9(2)V9(3) VALUE ZERO.

       PROCEDURE DIVISION.
       BEGIN.
           OPEN INPUT SCORE-FILE
           OPEN OUTPUT AVG-FILE 
           PERFORM UNTIL END-OF-SCORE-FILE 
                   READ SCORE-FILE 
                   AT END
                      SET END-OF-SCORE-FILE TO TRUE 
                   END-READ
                   
                   IF NOT END-OF-SCORE-FILE THEN
                      PERFORM 001-PROCESS THRU 001-EXIT

                    IF NOT END-OF-SCORE-FILE AND COURSE-ID(1:1) IS EQUAL
                      TO "3" THEN
                      PERFORM 002-PROCESS THRU 002-EXIT 
                    IF NOT END-OF-SCORE-FILE AND COURSE-ID(1:2) IS EQUAL
                      TO "31" THEN
                      PERFORM 003-PROCESS THRU 003-EXIT
                   
           END-PERFORM
           DISPLAY "AVG-GRADE : " AVG-GRADE
           DISPLAY "AVG-SCI-GRADE : " AVG-SCI-GRADE
           DISPLAY "AVG-CS-GRADE  : " AVG-CS-GRADE
           
           MOVE AVG-GRADE TO AVG-GRADE1 IN AVG-DETAIL
           WRITE AVG-DETAIL
           MOVE AVG-SCI-GRADE TO AVG-GRADE1 IN AVG-DETAIL
           WRITE AVG-DETAIL
           MOVE AVG-CS-GRADE TO AVG-GRADE1 IN AVG-DETAIL
           WRITE AVG-DETAIL
           CLOSE AVG-FILE
           CLOSE SCORE-FILE
           GOBACK 
           .

       001-PROCESS.
           
           EVALUATE TRUE 
           WHEN GRADE = "D"
                COMPUTE SCORE = CREDIT * 1
           WHEN GRADE = "D+"
                COMPUTE SCORE = CREDIT * 1.5
           WHEN GRADE = "C"
                COMPUTE SCORE = CREDIT * 2
           WHEN GRADE = "C+"
                COMPUTE SCORE = CREDIT * 2.5
           WHEN GRADE = "B"
                COMPUTE SCORE = CREDIT * 3
           WHEN GRADE = "B+"
                COMPUTE SCORE = CREDIT * 3.5
           WHEN GRADE = "A"
                COMPUTE SCORE = CREDIT * 4
           END-EVALUATE
           COMPUTE SCORE-ALL = SCORE-ALL + SCORE
           COMPUTE SUM-UNIT = SUM-UNIT + CREDIT
           COMPUTE AVG-GRADE = SCORE-ALL / SUM-UNIT

           
           .




       001-EXIT.
           EXIT.

       002-PROCESS.
           
           EVALUATE TRUE 
           WHEN GRADE = "D"
                COMPUTE SCORE2 = CREDIT * 1
           WHEN GRADE = "D+"
                COMPUTE SCORE2 = CREDIT * 1.5
           WHEN GRADE = "C"
                COMPUTE SCORE2 = CREDIT * 2
           WHEN GRADE = "C+"
                COMPUTE SCORE2 = CREDIT * 2.5
           WHEN GRADE = "B"
                COMPUTE SCORE2 = CREDIT * 3
           WHEN GRADE = "B+"
                COMPUTE SCORE2 = CREDIT * 3.5
           WHEN GRADE = "A"
                COMPUTE SCORE2 = CREDIT * 4
           END-EVALUATE
           COMPUTE SCORE-ALL2 = SCORE-ALL2 + SCORE2
           COMPUTE SUM-UNIT2 = SUM-UNIT2 + CREDIT
           COMPUTE AVG-SCI-GRADE = SCORE-ALL2 / SUM-UNIT2

           
           .




       002-EXIT.
           EXIT.

       003-PROCESS.
           
           EVALUATE TRUE 
           WHEN GRADE = "D"
                COMPUTE SCORE3 = CREDIT * 1
           WHEN GRADE = "D+"
                COMPUTE SCORE3 = CREDIT * 1.5
           WHEN GRADE = "C"
                COMPUTE SCORE3 = CREDIT * 2
           WHEN GRADE = "C+"
                COMPUTE SCORE3 = CREDIT * 2.5
           WHEN GRADE = "B"
                COMPUTE SCORE3 = CREDIT * 3
           WHEN GRADE = "B+"
                COMPUTE SCORE3 = CREDIT * 3.5
           WHEN GRADE = "A"
                COMPUTE SCORE3 = CREDIT * 4
           END-EVALUATE
           COMPUTE SCORE-ALL3 = SCORE-ALL3 + SCORE3
           COMPUTE SUM-UNIT3 = SUM-UNIT3 + CREDIT
           COMPUTE AVG-CS-GRADE = SCORE-ALL3 / SUM-UNIT3

           
           .




       003-EXIT.
           EXIT.